import threading
from pymongo import MongoClient
from selenium import webdriver
from selenium.webdriver.common.action_chains import ActionChains
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
import time


settings = {
    "chrome_driver": "./chromedriver",
    "search_word": "Бетон",
    "latitude_begin": 69.00,
    "latitude_end": 43.00,
    "longitude_begin": 28.00,
    "longitude_end": 180.00,
    "company_list": ".miniCard__header", 
    "btn_close_detail": ".frame._num_2 .link.frame__controlsButton._close._undashed",
    "title": "cardHeader__headerNameText", 
    "address_street": "card__addressLink",
    "address_sity": "_purpose_drilldown",
    "url": 'contact__linkText',
    "btn_open_phone": ".contact__toggle._place_phones", 
    "phone": 'a.contact__phonesItemLink .contact__phonesItemLinkNumber', 
    "btn_open_email": ".contact__toggle._place_other",
    "email": "._type_email .link.contact__linkText",
    "btn_pagination_next": ".pagination__arrow._right",
    "btn_pagination_next_disable": ".pagination__arrow._right._disabled",
    "no_result": "searchResults__nothingFoundHeader", 
    "btn_search_this_area": 'label[title="На карте"]', 
    "btn_submit": ".searchBar__submit._directory",
}

NO_SAVE_URL = ('Вконтакте', 'Facebook', 'Google+', 'Twitter', 'Организация в Instagram', '', 'Instagram', 'Организация во Вконтакте')

class Parse2GisMap():
    def __init__(self, search_word=settings["search_word"]):
        self.template_url = "https://2gis.ru/search/{search_word}/firms/bound?queryState=center%2F{longitude}%2C{latitude}%2Fzoom%2F8"
        self.search_word = search_word
        mongo = MongoClient('127.0.0.1', 27017, connect = False)
        db = mongo.companies_db
        self.company = db.two_gis_companies
        self.lock = threading.Lock()
        options = webdriver.ChromeOptions()
        options.add_argument('--headless')
        options.add_argument('--disable-gpu')
        chrome = webdriver.Chrome(executable_path=settings['chrome_driver'], chrome_options=options)
        chrome.implicitly_wait(2)
        chrome.set_window_size(1920,1080)
        chrome.maximize_window()
        self.chrome = chrome
        self.wait_element = WebDriverWait(self.chrome, 1)

    def __move_and_click(self, element, pause_time):
        ActionChains(self.chrome).move_to_element(element).pause(pause_time).click().perform()


    def __save_company(self, company):
        with self.lock:
            self.company.save(company)
            print("save: {}".format(company))

    def __get_company_detail(self):
        try:
            title = WebDriverWait(self.chrome, 4).until(EC.presence_of_element_located((By.CLASS_NAME, settings['title']))).text
            try:
                address_street = self.chrome.find_element_by_class_name(settings['address_street']).text
                address_sity = self.chrome.find_element_by_class_name(settings['address_sity']).text
                address = "{}, {}".format(address_sity, address_street)
            except:
                address = ""
            try:
                urls = self.chrome.find_elements_by_class_name(settings['url'])
                url_list = [url.text for url in urls if not url.text in NO_SAVE_URL]
            except:
                url_list = []
            try:
                btn_open_phone = self.chrome.find_element_by_css_selector(settings['btn_open_phone'])
                btn_open_phone.click()
                phones = self.chrome.find_elements_by_css_selector(settings['phone'])
                phone_list = [phone.text for phone in phones]
            except:
                phone_list = []
            try:
                btn_open_email = self.chrome.find_element_by_css_selector(settings['btn_open_email'])
                btn_open_email.click()
                emails = self.chrome.find_element_by_css_selector(settings['email'])
                emails_list = [email.text for email in emails]
            except:
                emails_list = []
            company = {
                "title": title,
                "address": address,
                "url": url_list,
                "phone": phone_list,
                "email": emails_list,
            }
            return company
        except:
            return {}


    def __save_company(self, company):
        with self.lock:
            self.company.save(company)
            print("save: {}".format(company))


    def __is_in_base(self, company_detail):
        with self.lock:
            is_in_base = self.company.find_one(company_detail)
            if is_in_base:
                print("организация уже в базе")
        return bool(is_in_base)


    def __get_company_list_and_save(self):
        try:
            company_list = self.wait_element.until(EC.presence_of_all_elements_located((By.CSS_SELECTOR, settings['company_list'])))
        except:
            self.chrome.refresh()
            company_list = self.wait_element.until(EC.presence_of_all_elements_located((By.CSS_SELECTOR, settings['company_list'])))
        for company in company_list:
            self.__move_and_click(company, 1)
            company_detail = self.__get_company_detail()
            if not self.__is_in_base(company_detail):
                self.__save_company(company_detail)
            btn_close_detal = self.wait_element.until(EC.element_to_be_clickable((By.CSS_SELECTOR, settings['btn_close_detail'])))
            btn_close_detal.click()
            

    def __pagination_on_page(self):
        try:
            btn_pagination_next = self.wait_element.until(EC.element_to_be_clickable((By.CSS_SELECTOR, settings['btn_pagination_next'])))
            while btn_pagination_next:
                self.__move_and_click(btn_pagination_next, 1)
                self.__get_company_list_and_save()
                try:
                    btn_pagination_next = self.wait_element.until(EC.element_to_be_clickable((By.CSS_SELECTOR, settings['btn_pagination_next'])))
                    print("следующая страница с компаниями")
                except:
                    break
                try:
                   btn_pagination_next_disable = self.wait_element.until(EC.presence_of_element_located((By.CSS_SELECTOR, settings['btn_pagination_next_disable'])))
                   if btn_pagination_next_disable:
                       break
                except:
                    pass 
        except:
            pass

    
    def _check_page_by_company(self, num=1, latitude_begin=69.00, latitude_end=43.00, longitude_begin=31.00, longitude_end=180.00):
        def next_step(latitude, longitude):
            if longitude <= longitude_end:
                longitude += 7.6
            elif longitude >= longitude_end:
                longitude = longitude_begin
                if latitude > 69.2:
                    latitude -= 1.4
                elif 67.54 < latitude <= 69.2:
                    latitude -= 1.5
                elif 65.85 < latitude <= 67.54:
                    latitude -= 1.6
                elif 64.0 < latitude <= 65.85:
                    latitude -= 1.7
                elif 62.0 < latitude <= 64.0:
                    latitude -= 1.8
                elif 59.9 < latitude <= 62.0:
                    latitude -= 1.9
                elif 57.6 < latitude <= 59.9:
                    latitude -= 2.0
                elif 55.2 < latitude <= 57.6:
                    latitude -= 2.1
                elif 52.6 < latitude <= 55.2:
                    latitude -= 2.2
                elif 49.9 < latitude <= 52.6:
                    latitude -= 2.3
                elif 47.0 < latitude <= 49.9:
                    latitude -= 2.4
                elif 44.0 < latitude <= 47.0:
                    latitude -= 2.5
                elif 40.8 < latitude <= 44.0:
                    latitude -= 2.6 
                else:
                    latitude -= 3.0
            return latitude, longitude

        latitude, longitude = (latitude_begin, longitude_begin)
        with self.lock:
            is_last_point = self.company.find({"last_point_{}".format(num): True})
        if is_last_point:
            for point in is_last_point:
                latitude, longitude = point.get("coordinate")
        if longitude > longitude_end:
            latitude, longitude = next_step(latitude, longitude)
        if latitude <= latitude_end:
            self.chrome.quit()
            print('Поток-{}: Сбор информации окончен'.format(num))
        while latitude > latitude_end:
            
            query = {"last_point_{}".format(num): True}
            data = {"last_point_{}".format(num): True, "coordinate": [latitude, longitude]}
            with self.lock:
                self.company.update(query, data, upsert=True)
            url = self.template_url.format(longitude=str(longitude), latitude=str(latitude), search_word=str(self.search_word))
            print('Поток-{}: {}'.format(num, url))
            try:
                self.chrome.get(url)
                btn_submit = WebDriverWait(self.chrome, 4).until(EC.presence_of_element_located((By.CSS_SELECTOR, settings['btn_submit'])))
            except:
                self.chrome.refresh()
            print("Поток-{}: открываем страницу".format(num))
            
            try:
                no_result = WebDriverWait(self.chrome, 4).until(EC.presence_of_element_located((By.CLASS_NAME, settings['no_result'])))
                latitude, longitude = next_step(latitude, longitude)
                print("Поток-{}: нет данных по запросу '{}' на карте.".format(num, self.search_word))
                if latitude <= latitude_end:
                    self.chrome.quit()
                    print('Поток-{}: Сбор информации окончен'.format(num))
                continue
            except:
                print("Поток-{}: нашли элементы".format(num))
                self.__get_company_list_and_save()
                self.__pagination_on_page()
                latitude, longitude = next_step(latitude, longitude)
            
            if latitude <= latitude_end:
                self.chrome.quit()
                print('Поток-{}: Сбор информации окончен'.format(num))
            


def __select_number_threads():
    import multiprocessing
    cpu_count = int(multiprocessing.cpu_count())
    if cpu_count > 4:
        threads = cpu_count - 2
    elif 1 < cpu_count <= 4:
        threads = cpu_count - 1
    else:
        threads = cpu_count
    return threads

def get_targets_and_params():
    step = __select_number_threads()
    latitude_begin = settings["latitude_begin"]
    latitude_end = settings["latitude_end"]
    len_latitude = latitude_begin - latitude_end
    step_for_thread = len_latitude/step
    params = []
    target =[]
    curr_latitude_begin = latitude_begin
    curr_latitude_end = latitude_begin - step_for_thread
    num = 1
    
    while curr_latitude_begin - step_for_thread > latitude_end  and curr_latitude_end > latitude_end:
        two_gis_parser = Parse2GisMap()
        params.append((num, curr_latitude_begin, curr_latitude_end, settings["longitude_begin"], settings["longitude_end"],))
        target.append(two_gis_parser._check_page_by_company)
        num += 1
        curr_latitude_begin = curr_latitude_end
        curr_latitude_end = curr_latitude_begin - step_for_thread
    return zip(target, params)


def main():
    targets_and_params = get_targets_and_params()
   
    for target, param in targets_and_params:
        print(param)
        my_thread = threading.Thread(target=target, args=(*param,))
        my_thread.start()

if __name__ == "__main__":
    main()